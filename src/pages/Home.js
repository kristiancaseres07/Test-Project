import {Col, Row, Container, Button, Card} from 'react-bootstrap';
import { useState, useEffect } from 'react';
import Banner from '../components/Banner';
import WhoWeAre from '../components/WhoWeAre';
import OurServices from '../components/OurServices';
import AboutUs from '../components/AboutUs';
import LetsDoThis from '../components/LetsDoThis';
import Service1 from '../images/services (1).jpg'  
import Service2 from '../images/services (2).jpg'
import Service3 from '../images/services (3).jpg'
import Service4 from '../images/services (4).jpg'

export default function Home () {

    const [getServices, setServices] = useState([])




    const data = [
    {
      imgUrl: `${Service1}`,
      title: 'WEB DEVELOPMENT',
      description: 'Start and grow your business with your own website, tool and app.' 
    },
    {
      imgUrl: `${Service2}`,
      title: 'VIDEO and PHOTO PRODUCTION',
      description: 'Stand out with amazing visuals to send your message across.' 
    },
    {
      imgUrl: `${Service3}`,
      title: 'SOCIAL MEDIA MANAGEMENT',
      description: 'Engage your audience with content tailor-fit to your brand.' 
    },
    {
      imgUrl: `${Service4}`,
      title: 'COMMUNITY MANAGEMENT',
      description: 'Nurture your community as they grow with you through engagements.' 
    },
    ]





  useEffect(() => {


      setServices(data.map(service => {
    return (
        <OurServices serviceProp={service}/>
      )
    }))


}, [])

  return (
    <div>
      <Banner />
      <WhoWeAre />
          <Container fluid className="midnightBlue" id="services">
            <h1 className="pt-5 pb-3 ourServices">Our Services</h1>
              <Container style={{padding: "0px 5% 100px 5%"}}>
                <Row>
                  {getServices}
                </Row>
              </Container>
          </Container>
      <AboutUs />
      <LetsDoThis />
    </div>
  );
};


